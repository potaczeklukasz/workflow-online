# WorkFlow Online

A simple application for managing employees and purchased modules.

## Table of Contents
* [General info](#general-info)
* [Technologies](#technologies)
* [Setup](#setup)

## General info
The application enables registering a company, adding employees and activating modules used by the company.

|  Register | Modules view  |
| --------  | --------  |
|  ![Register](https://i.ibb.co/QfVZWC5/register.png "Register") |  ![Modules view](https://i.ibb.co/Xs83rcj/modules.png "Modules view") |
|  **Employess list** | **Expanded employee details**  |
|  ![Employess list](https://i.ibb.co/qy3wxL0/user-list.png "Employess list") |  ![Expanded employee details](https://i.ibb.co/gRYL5n9/user-list2.png "Expanded employee details") |
|  **Home page** | **Edit details**  |
|  ![Home page](https://i.ibb.co/0qQGKsr/Home-page.png "Home page") |  ![Edit details](https://i.ibb.co/vxkF0pH/Edit-details.png "Edit details") |

## Technologies
- ASP.NET Core 2.1 (server API)
- React 16.4.2
- Redux 4.0.0

## Setup
To run this project, download [API](https://bitbucket.org/potaczeklukasz/workflow-online-backend/src/master/ "API") and run it:

```
$ dotnet run
```

To run frontend:
```
$ npm install
$ npm start
```
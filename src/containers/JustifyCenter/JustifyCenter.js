import React from 'react';

import classes from './JustifyCenter.css';

const justifyCenter = (props) => {
    return (
        <div className={classes.JustifyCenter}>
            {props.children}
        </div>
    );
}

export default justifyCenter;
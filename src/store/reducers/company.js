import * as actionTypes from '../actions/actionTypes';
import { updateObject } from '../../helpers/helpers';

const initialState = {
    companyId: null,
    companyName: null,
    companyEmail: null,
    companyNIP: null,
    companyAddress: null
}

const reducer = (state = initialState, action) => {
    switch (action.type) {
        case actionTypes.COMPANY_SET_COMPANY_DETAILS:
            return updateObject(state, {
                companyId: action.companyId,
                companyName: action.companyName,
                companyEmail: action.companyEmail,
                companyNIP: action.companyNIP,
                companyAddress: action.companyAddress
            });
        case actionTypes.COMPANY_EDIT_COMPANY_DETAILS:
            return updateObject(state, {
                companyName: action.companyName,
                companyEmail: action.email,
                companyNIP: action.NIP,
                companyAddress: action.address
            });
        default:
            return state;
    }
}

export default reducer;
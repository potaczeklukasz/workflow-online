import * as actionTypes from '../actions/actionTypes';
import { updateObject } from '../../helpers/helpers';

const initialState = {
    token: null,
    userId: null,
    companyId: null,
    firstName: null,
    lastName: null,
    position: null,
    picture: null,
    email: null
}

const reducer = (state = initialState, action) => {
    switch (action.type) {
        case actionTypes.AUTH_GET_TOKEN:
            return updateObject(state, {
                token: action.token,
                userId: action.userId
            });
        case actionTypes.AUTH_LOGOUT:
            return updateObject(state, {
                token: null,
                userId: null
            });
        case actionTypes.AUTH_SET_TOKEN_FROM_COOKIE:
            return updateObject(state, {
                token: action.token,
                userId: action.userId
            });
        case actionTypes.AUTH_SET_USER_DATA:
            return updateObject(state, {
                companyId: action.companyId,
                firstName: action.firstName,
                lastName: action.lastName,
                position: action.position,
                email: action.email,
                picture: action.picture
            });
        case actionTypes.AUTH_EDIT_USER_DATA:
            return updateObject(state, {
                firstName: action.firstName,
                lastName: action.lastName,
                position: action.position,
                email: action.email,
                picture: action.picture
            });
        default:
            return state;
    }
}

export default reducer;
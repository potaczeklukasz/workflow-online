import * as actionTypes from '../actions/actionTypes';

export const setCompanyDetails = (companyId, companyName, companyEmail, companyNIP, companyAddress) => {
    return {
        type: actionTypes.COMPANY_SET_COMPANY_DETAILS,
        companyId,
        companyName,
        companyEmail,
        companyNIP,
        companyAddress
    }
}

export const editCompanyData = (companyName, email, address, NIP) => {
    return {
        type: actionTypes.COMPANY_EDIT_COMPANY_DETAILS,
        companyName,
        email,
        address,
        NIP
    }
}
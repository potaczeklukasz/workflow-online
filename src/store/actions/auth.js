import * as actionTypes from '../actions/actionTypes';

export const login = (token, userId) => {
    return {
        type: actionTypes.AUTH_GET_TOKEN,
        token,
        userId
    }
}

export const logout = () => {
    return {
        type: actionTypes.AUTH_LOGOUT
    }
}

export const setTokenFromCookie = (token, userId) => {
    return {
        type: actionTypes.AUTH_SET_TOKEN_FROM_COOKIE,
        token,
        userId
    }
}

export const editUserData = (firstName, lastName, position, email, picture) => {
    return {
        type: actionTypes.AUTH_EDIT_USER_DATA,
        firstName,
        lastName,
        position,
        email,
        picture
    }
}

export const setUserData = (companyId, firstName, lastName, position, email, picture) => {
    return {
        type: actionTypes.AUTH_SET_USER_DATA,
        companyId,
        firstName,
        lastName,
        position,
        email,
        picture
    }
}
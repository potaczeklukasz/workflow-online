export const saveCookie = (auth_token, userId, hours) => {
    var now = new Date();
    var time = now.getTime();
    time += hours * 60 * 60 * 1000;
    now.setTime(time);
    document.cookie =
        'auth_token=' + auth_token +
        '; expires=' + now.toUTCString() +
        '; path=/';
    document.cookie =
        'userId=' + userId +
        '; expires=' + now.toUTCString() +
        '; path=/';
}

export const getCookies = () => {
    const cookies = {};
    for (const cookie of document.cookie.split(";")) {
        cookies[cookie.split("=")[0].trim()] = cookie.split("=")[1];
    }
    return cookies;
}

export const deleteAuthCookie = () => {
    document.cookie = "auth_token=; expires=Thu, 01 Jan 1970 00:00:00 UTC; path=/;";
    document.cookie = "userId=; expires=Thu, 01 Jan 1970 00:00:00 UTC; path=/;";
}
import React, { Component } from 'react';
import { connect } from 'react-redux';
import { Redirect, withRouter } from 'react-router-dom';
import axios from 'axios';

import Input from '../UI/Input/Input';
import Button from '../UI/Button/Button';
import JustifyCenter from '../../containers/JustifyCenter/JustifyCenter';

import classes from './Register.css';

import { updateObject, checkValidity } from "../../helpers/helpers";
import { saveCookie, getCookies } from '../../helpers/cookies';
import * as actionCreators from '../../store/actions/index';

class Register extends Component {
    state = {
        controls: {
            login: {
                elementType: 'input',
                elementConfig: {
                    type: 'email',
                    placeholder: 'Mail address'
                },
                value: '',
                label: 'User details:',
                validation: {
                    required: true,
                    isEmail: true
                },
                valid: false,
                touched: false
            },
            password: {
                elementType: 'input',
                elementConfig: {
                    type: 'password',
                    placeholder: 'Password'
                },
                value: '',
                validation: {
                    required: true
                },
                valid: false,
                touched: false
            },
            firstName: {
                elementType: 'input',
                elementConfig: {
                    type: 'input',
                    placeholder: 'First name'
                },
                value: '',
                validation: {
                    required: true
                },
                valid: false,
                touched: false
            },
            lastName: {
                elementType: 'input',
                elementConfig: {
                    type: 'input',
                    placeholder: 'Last name'
                },
                value: '',
                validation: {
                    required: true
                },
                valid: false,
                touched: false
            },
            position: {
                elementType: 'input',
                elementConfig: {
                    type: 'input',
                    placeholder: 'Position'
                },
                value: '',
                validation: {
                    required: true
                },
                valid: false,
                touched: false
            },
            companyName: {
                elementType: 'input',
                elementConfig: {
                    type: 'input',
                    placeholder: 'Company name'
                },
                value: '',
                label: 'Company Details:',
                validation: {
                    required: true
                },
                valid: false,
                touched: false
            },
            companyEmail: {
                elementType: 'input',
                elementConfig: {
                    type: 'email',
                    placeholder: 'Contact email to company'
                },
                value: '',
                validation: {
                    required: true,
                    isEmail: true
                },
                valid: false,
                touched: false
            },
            companyNIP: {
                elementType: 'input',
                elementConfig: {
                    type: 'input',
                    placeholder: 'NIP'
                },
                value: '',
                validation: {
                    required: true
                },
                valid: false,
                touched: false
            },
            companyAddress: {
                elementType: 'textarea',
                elementConfig: {
                    placeholder: 'Company address: Street, City, Postal Code'
                },
                value: '',
                validation: {
                    required: true
                },
                valid: false,
                touched: false
            }
        },
        error: null
    }

    inputChangedHandler = (event, controlName) => {
        const updatedControls = updateObject(this.state.controls, {
            [controlName]: updateObject(this.state.controls[controlName], {
                value: event.target.value,
                valid: checkValidity(event.target.value, this.state.controls[controlName].validation),
                touched: true
            })
        });
        this.setState({ controls: updatedControls });
    }

    login = async () => {
        try {
            const response = await axios.post('http://localhost:5000/api/auth/login', {
                userName: this.state.controls.login.value,
                password: this.state.controls.password.value
            });

            if (response.status === 200) {
                this.props.login(response.data.auth_token, response.data.id);
                saveCookie(response.data.auth_token, response.data.id, 2);
            }
        } catch (err) {
            this.setState({ error: err.response.data.error });
        }
    }

    submitHandler = async (event) => {
        event.preventDefault();
        if (this.checkIfValid()) {
            try {
                const response = await axios.post('http://localhost:5000/api/accounts/register', {
                    email: this.state.controls.login.value,
                    password: this.state.controls.password.value,
                    firstName: this.state.controls.firstName.value,
                    lastName: this.state.controls.lastName.value,
                    position: this.state.controls.position.value,
                    companyName: this.state.controls.companyName.value,
                    companyEmail: this.state.controls.companyEmail.value,
                    companyNIP: this.state.controls.companyNIP.value,
                    companyAddress: this.state.controls.companyAddress.value
                });
                if (response.status === 201) {
                    this.login();
                }
            } catch (err) {
                let errors = "";
                for (const e in err.response.data) {
                    errors += `${err.response.data[e]} \n`;
                }
                this.setState({ error: errors });
            }
        } else {
            this.setState({ error: "Please fill in all fields" });
        }
    }

    checkIfValid = () => {
        let isValid = true;
        for (const input in this.state.controls) {
            if (!this.state.controls[input].valid) isValid = false;
        }
        return isValid;
    }

    render() {
        const cookies = getCookies();
        const formElementsArray = [];
        for (let key in this.state.controls) {
            formElementsArray.push({
                id: key,
                config: this.state.controls[key]
            });
        }

        let form = formElementsArray.map(formElement => (
            <Input
                label={formElement.config.label}
                key={formElement.id}
                elementType={formElement.config.elementType}
                elementConfig={formElement.config.elementConfig}
                value={formElement.config.value}
                invalid={!formElement.config.valid}
                shouldValidate={formElement.config.validation}
                touched={formElement.config.touched}
                onChange={(event) => this.inputChangedHandler(event, formElement.id)} />
        ));

        return (
            <JustifyCenter>
                <div className={classes.Register}>
                    {this.props.isAuthenticated || cookies.auth_token ? <Redirect to="/" /> : null}
                    <form onSubmit={this.submitHandler}>
                        {form}
                        <Button btnType="Green">Register</Button>
                        <p className={classes.Error}>{this.state.error}</p>
                    </form>
                </div>
            </JustifyCenter>
        );
    }
}

const mapStateToProps = state => {
    return {
        isAuthenticated: state.auth.token !== null
    };
};

const mapDispatchToProps = dispatch => {
    return {
        login: (token, userId) => dispatch(actionCreators.login(token, userId))
    };
};

export default withRouter(connect(mapStateToProps, mapDispatchToProps)(Register));
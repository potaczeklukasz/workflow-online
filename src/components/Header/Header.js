import React, { Component } from 'react';
import { connect } from 'react-redux';
import { Link } from 'react-router-dom';

import Button from '../UI/Button/Button';

import classes from './Header.css';
import menu from '../../gfx/menu.png';

import { deleteAuthCookie } from '../../helpers/cookies';
import * as actionCreators from '../../store/actions/index';

class Header extends Component {
    state = {
        showSidebar: window.innerWidth < 950 ? false : true
    }

    componentDidMount() {
        window.addEventListener("resize", () => {
            window.innerWidth < 950 ? this.setState({ showSidebar: false }) : this.setState({ showSidebar: true });
        });
    }

    logout = () => {
        this.props.logout();
        deleteAuthCookie();
    }

    render() {
        let buttons = (
            <div>
                <Link to="/register"><Button btnType="Blue">Register</Button></Link>
                <Link to="/login"><Button btnType="Blue">Login</Button></Link>
            </div>
        );

        if (this.props.isAuthenticated) buttons = <Button btnType="Red" clicked={this.logout}>Log out</Button>;

        return (
            <div className={classes.Header}>
                <img style={window.innerWidth > 950 ? { display: "none" } : null} onClick={this.props.onClick} className={classes.MenuImg} src={menu} alt="menu" />
                {buttons}
            </div>
        );
    }
}

const mapStateToProps = state => {
    return {
        isAuthenticated: state.auth.token !== null
    };
};

const mapDispatchToProps = dispatch => {
    return {
        logout: () => dispatch(actionCreators.logout())
    };
};

export default connect(mapStateToProps, mapDispatchToProps)(Header);
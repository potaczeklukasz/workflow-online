import React, { Component } from 'react';
import axios from 'axios';

import Input from '../UI/Input/Input';
import Button from '../UI/Button/Button';
import JustifyCenter from '../../containers/JustifyCenter/JustifyCenter';

import classes from './Password.css';

import { updateObject, checkValidity } from "../../helpers/helpers";

class Password extends Component {
    state = {
        controls: {
            email: {
                elementType: 'input',
                label: "We'll send you an email to reset your password.",
                elementConfig: {
                    type: 'email',
                    placeholder: 'Enter your email'
                },
                value: '',
                validation: {
                    required: true,
                    isEmail: true
                },
                valid: false,
                touched: false
            }
        },
        error: null,
        send: false
    }

    inputChangedHandler = (event, controlName) => {
        const updatedControls = updateObject(this.state.controls, {
            [controlName]: updateObject(this.state.controls[controlName], {
                value: event.target.value,
                valid: checkValidity(event.target.value, this.state.controls[controlName].validation),
                touched: true
            })
        });
        this.setState({ controls: updatedControls });
    }

    submitHandler = async (event) => {
        event.preventDefault();
        try {
            const response = await axios.get(`http://localhost:5000/api/accounts/password/${this.state.controls.email.value}`);
            if (response.status === 201) {
                this.setState({ send: true });
            }
        } catch (err) {
            this.setState({ error: err.response.error });
        }
    }

    render() {
        const formElementsArray = [];
        for (let key in this.state.controls) {
            formElementsArray.push({
                id: key,
                config: this.state.controls[key]
            });
        }

        let form = formElementsArray.map(formElement => (
            <Input
                key={formElement.id}
                elementType={formElement.config.elementType}
                label={formElement.config.label}
                elementConfig={formElement.config.elementConfig}
                value={formElement.config.value}
                invalid={!formElement.config.valid}
                shouldValidate={formElement.config.validation}
                touched={formElement.config.touched}
                onChange={(event) => this.inputChangedHandler(event, formElement.id)} />
        ));

        const displayForm = (
            <form onSubmit={this.submitHandler}>
                {form}
                <Button btnType="Green">Send</Button>
                <p className={classes.Error}>{this.state.error}</p>
            </form>
        );

        return (
            <JustifyCenter>
                <div className={classes.Password}>
                    {this.state.send ? <p> Check your email </p> : displayForm}
                </div>
            </JustifyCenter>
        );
    }
}

export default Password;

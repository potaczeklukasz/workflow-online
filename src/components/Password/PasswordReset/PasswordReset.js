import React, { Component } from 'react';
import { Redirect, withRouter } from 'react-router-dom';
import axios from 'axios';
import queryString from 'querystring';

import Input from '../../UI/Input/Input';
import Button from '../../UI/Button/Button';
import JustifyCenter from '../../../containers/JustifyCenter/JustifyCenter';

import classes from './PasswordReset.css';

import { updateObject, checkValidity } from "../../../helpers/helpers";

class Password extends Component {
    state = {
        controls: {
            password: {
                elementType: 'input',
                label: "Enter your new password:",
                elementConfig: {
                    type: 'password',
                    placeholder: 'Enter your new password'
                },
                value: '',
                validation: {
                    required: true
                },
                valid: false,
                touched: false
            }
        },
        error: null,
        active: false,
        send: false,
        redirect: false
    }

    async componentDidMount() {
        const parsed = queryString.parse(window.location.search);
        const id = parsed["?id"];
        try {
            const response = await axios.get(`http://localhost:5000/api/accounts/password/reset/${id}`);
            if (response.status === 200) {
                this.setState({ active: true });
            }
        } catch (err) {
            this.setState({ error: err.response.data.login_failure });
        }
    }

    inputChangedHandler = (event, controlName) => {
        const updatedControls = updateObject(this.state.controls, {
            [controlName]: updateObject(this.state.controls[controlName], {
                value: event.target.value,
                valid: checkValidity(event.target.value, this.state.controls[controlName].validation),
                touched: true
            })
        });
        this.setState({ controls: updatedControls });
    }

    submitHandler = async (event) => {
        event.preventDefault();
        const parsed = queryString.parse(window.location.search);
        const id = parsed["?id"];
        try {
            const response = await axios.post(`http://localhost:5000/api/accounts/password/new`, {
                password: this.state.controls.password.value,
                id
            });
            if (response.status === 200) {
                this.setState({ send: true });
                setTimeout(() => this.setState({ redirect: true }), 3000);
            }
        } catch (err) {
            this.setState({ error: err.response.data.login_failure });
        }
    }

    render() {
        const formElementsArray = [];
        for (let key in this.state.controls) {
            formElementsArray.push({
                id: key,
                config: this.state.controls[key]
            });
        }

        let form = formElementsArray.map(formElement => (
            <Input
                key={formElement.id}
                elementType={formElement.config.elementType}
                label={formElement.config.label}
                elementConfig={formElement.config.elementConfig}
                value={formElement.config.value}
                invalid={!formElement.config.valid}
                shouldValidate={formElement.config.validation}
                touched={formElement.config.touched}
                onChange={(event) => this.inputChangedHandler(event, formElement.id)} />
        ));

        const displayForm = (
            <form onSubmit={this.submitHandler}>
                {form}
                <Button btnType="Green">Send</Button>
                <p className={classes.Error}>{this.state.error}</p>
            </form>
        );

        return (
            <JustifyCenter>
                <div className={classes.PasswordReset}>
                    {this.state.active ? displayForm : <p> The link is incorrect or invalid </p>}
                    {this.state.send ? <p>Password has been changed. You will be redirected to the login page. </p> : null}
                    {this.state.redirect ? <Redirect to="/login" /> : null}
                </div>
            </JustifyCenter>
        );
    }
}

export default withRouter(Password);

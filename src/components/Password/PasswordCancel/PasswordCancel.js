import React, { Component } from 'react';
import queryString from 'querystring';
import axios from 'axios';

import classes from './PasswordCancel.css';

class PasswordCancel extends Component {
    async componentDidMount() {
        const parsed = queryString.parse(window.location.search);
        const id = parsed["?id"];
        try {
            await axios.get(`http://localhost:5000/api/accounts/password/cancel/${id}`);
        } catch (err) {
            console.log(err);
        }
    }

    render() {
        return (
            <div className={classes.PasswordCancel}>
                <p> The password reset link has been deactivated. </p>
            </div>
        );
    }
}

export default PasswordCancel;
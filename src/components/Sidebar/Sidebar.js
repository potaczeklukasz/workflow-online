import React from 'react';

import Logo from './Logo/Logo';
import Navigation from './Navigation/Navigation';
import UserData from './UserData/UserData';

import classes from './Sidebar.css';

const sidebar = (props) => {
    return (
        <div className={classes.Sidebar} style={props.style}>
            <Logo />
            <UserData />
            <Navigation />
        </div>
    )
}

export default sidebar;
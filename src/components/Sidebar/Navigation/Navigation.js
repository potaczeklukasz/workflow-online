import React from 'react';
import { NavLink } from 'react-router-dom';

import homeImg from '../../../gfx/home.png';
import usersImg from '../../../gfx/users.png';
import modulesImg from '../../../gfx/modules.png';

import classes from './Navigation.css';

const navigation = () => (
    <div className={classes.Navigation}>
        <NavLink activeClassName={classes.active} exact className={classes.Link} to="/"><img src={homeImg} alt="home" className={classes.Icon} /><p className={classes.Text}>Home</p></NavLink>
        <NavLink activeClassName={classes.active} exact className={classes.Link} to="/users"><img src={usersImg} alt="users" className={classes.Icon} style={{marginTop: "7px"}}/><p className={classes.Text}>Users</p></NavLink>
        <NavLink activeClassName={classes.active} exact className={classes.Link} to="/modules"><img src={modulesImg} alt="modules" className={classes.Icon} /><p className={classes.Text}>Modules</p></NavLink>
    </div>
);

export default navigation;
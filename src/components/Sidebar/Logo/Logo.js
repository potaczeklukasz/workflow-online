import React from 'react';

import logoImg from '../../../gfx/logo.png';

import classes from './Logo.css';

const logo = () => (
    <div className={classes.Logo}>
        <img src={logoImg} className={classes.LogoImg} alt="logo"/>
    </div>
);

export default logo;
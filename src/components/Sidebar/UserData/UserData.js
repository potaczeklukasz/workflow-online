import React, { Component } from 'react';
import { connect } from 'react-redux';

import userImg from '../../../gfx/user.png';

import classes from './UserData.css';

import axios from 'axios';
import * as actionCreators from '../../../store/actions/index';

class UserData extends Component {
    async loadCompanyDetails() {
        try {
            const response = await axios({
                url: `http://localhost:5000/api/companies/${this.props.companyId}`,
                method: 'get',
                headers: { 'Authorization': "Bearer " + this.props.auth_token }
            });
            if (response.status === 200) {
                const data = response.data;
                this.props.setCompanyDetails(data.id, data.companyName, data.email, data.nip, data.address);
            }
        } catch (err) {
            console.log(err)
        }
    }

    async componentDidMount() {
        try {
            const response = await axios({
                url: `http://localhost:5000/api/users/user/${this.props.userId}`,
                method: 'get',
                headers: { 'Authorization': "Bearer " + this.props.auth_token }
            });
            if (response.status === 200) {
                this.props.setUserData(response.data.companyId, response.data.firstName, response.data.lastName, response.data.position, response.data.email, response.data.pictureUrl);
                this.loadCompanyDetails();
            }
        } catch (err) {
            console.log(err)
        }
    }

    render() {
        return (
            <div className={classes.UserData} >
                <img src={this.props.photo !== null ? `http://localhost:5000/api/images/${this.props.photo}` : userImg} className={classes.UserImg} alt="user" />
                <p className={classes.Label}> Name: </p>
                <p className={classes.Data}> {this.props.firstName} {this.props.lastName}</p>
                <p className={classes.Label}> Company: </p>
                <p className={classes.Data}> {this.props.companyName} </p>
                <p className={classes.Label}> Email: </p>
                <p className={classes.Data}> {this.props.email} </p>
            </div>
        );
    }
}

const mapStateToprops = state => {
    return {
        firstName: state.auth.firstName,
        lastName: state.auth.lastName,
        photo: state.auth.picture,
        companyName: state.company.companyName,
        email: state.auth.email,
        userId: state.auth.userId,
        auth_token: state.auth.token,
        companyId: state.auth.companyId
    };
};

const mapDispatchToProps = dispatch => {
    return {
        setUserData: (companyId, firstName, lastName, position, email, picture) => dispatch(actionCreators.setUserData(companyId, firstName, lastName, position, email, picture)),
        setCompanyDetails: (companyId, companyName, companyEmail, companyNIP, companyAddress) => dispatch(actionCreators.setCompanyDetails(companyId, companyName, companyEmail, companyNIP, companyAddress))
    };
};

export default connect(mapStateToprops, mapDispatchToProps)(UserData);
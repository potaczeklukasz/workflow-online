import React, { Component } from 'react';
import { connect } from 'react-redux';
import axios from 'axios';

import userImg from '../../../gfx/user.png';
import Button from '../../UI/Button/Button';
import ModalEdit from './ModalEdit/ModalEdit';
import UploadModal from './UploadModal/UploadModal';
import ModalBackground from '../../UI/ModalBackground/ModalBackground';
import Container from '../../../containers/Container/Container';

import classes from './UserDetails.css';

import * as actionCreators from '../../../store/actions/index';

class UserDetails extends Component {
    state = {
        modal: false,
        uploadModal: false
    }

    openEditModal = () => {
        this.setState({ modal: true });
    }

    openUploadModal = () => {
        this.setState({ uploadModal: true });
    }

    saveData = async (firstName, lastName, position, email) => {
        try {
            const response = await axios({
                url: `http://localhost:5000/api/users`,
                method: 'put',
                headers: { 'Authorization': "Bearer " + this.props.auth_token },
                data: {
                    id: this.props.id,
                    firstName,
                    lastName,
                    position,
                    email
                }
            });

            if (response.status === 204) {
                this.props.editUserData(firstName, lastName, position, email, this.props.picture);
                this.setState({ modal: false });
            }
        } catch (err) {
            console.log(err);
        }
    }

    cancelData = () => {
        this.setState({ modal: false, uploadModal: false });
    }

    submitImage = (name) => {
        this.cancelData();
        this.props.editUserData(this.props.firstName, this.props.lastName, this.props.position, this.props.email, name);
    }

    render() {
        return (
            <Container>
                <div className={classes.UserDetails}>
                    <img src={this.props.photo !== null ? `http://localhost:5000/api/images/${this.props.photo}` : userImg} className={classes.Photo} alt="user" />
                    <div className={classes.Content}>
                        <div className={classes.Details}>
                            <p className={classes.Data}> <span>Name:</span> {this.props.firstName} </p>
                            <p className={classes.Data}> <span>Surname:</span> {this.props.lastName} </p>
                            <p className={classes.Data}> <span>Position:</span> {this.props.position} </p>
                            <p className={classes.Data}> <span>Email:</span> {this.props.email} </p>
                        </div>
                        <div className={classes.Buttons} >
                            <Button className={classes.Button} btnType="Yellow" clicked={this.openUploadModal}> Upload photo </Button>
                            <Button className={classes.Button} btnType="Cyan" clicked={this.openEditModal}> Edit details </Button>
                        </div>
                    </div>
                </div>

                {this.state.modal ? <ModalEdit onCancel={this.cancelData} onSave={this.saveData} /> : null}
                {this.state.uploadModal ? <UploadModal id={this.props.id} submit={this.submitImage} cancel={this.cancelData} /> : null}
                {this.state.modal || this.state.uploadModal ? <ModalBackground clicked={this.cancelData} /> : null}
            </Container>
        );
    }
}

const mapStateToProps = state => {
    return {
        firstName: state.auth.firstName,
        lastName: state.auth.lastName,
        photo: state.auth.picture,
        position: state.auth.position,
        picture: state.auth.picture,
        email: state.auth.email,
        id: state.auth.userId,
        auth_token: state.auth.token
    };
};

const mapDispatchToProps = dispatch => {
    return {
        editUserData: (firstName, lastName, position, email, picture) => dispatch(actionCreators.editUserData(firstName, lastName, position, email, picture))
    };
};

export default connect(mapStateToProps, mapDispatchToProps)(UserDetails);
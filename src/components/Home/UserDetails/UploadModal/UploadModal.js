import React, { Component } from 'react';
import { connect } from 'react-redux';
import axios from 'axios';

import Button from '../../../UI/Button/Button';

import classes from './UploadModal.css';

class UploadModal extends Component {
    state = {
        selectedFile: null
    }

    fileSelectedHandler = e => {
        this.setState({ selectedFile: e.target.files[0] });
    }

    fileUploadHandler = async () => {
        if (this.state.selectedFile) {
            const formData = new FormData();
            formData.append('file', this.state.selectedFile, this.state.selectedFile.name);
            formData.append('id', this.props.id);
            try {
                const response = await axios({
                    url: `http://localhost:5000/api/images/upload`,
                    method: 'post',
                    headers: { 'Authorization': "Bearer " + this.props.auth_token },
                    data: formData
                });

                if (response.status === 200) {
                    this.props.submit(response.data.fileName);
                }
            } catch (err) {
                console.log(err);
            }
        } else {
            alert("Nie wczytano pliku");
        }
    }

    render() {
        return (
            <div className={classes.UploadModal} >
                <input type="file" name="file" onChange={this.fileSelectedHandler} accept="image/*" />
                <Button className={classes.Button} btnType="Green" clicked={this.fileUploadHandler}> Upload </Button>
                <Button className={classes.Button} btnType="Red" clicked={this.props.cancel}> Cancel </Button>
            </div>
        );
    }
}

const mapStateToProps = state => {
    return {
        auth_token: state.auth.token
    };
};

export default connect(mapStateToProps)(UploadModal);
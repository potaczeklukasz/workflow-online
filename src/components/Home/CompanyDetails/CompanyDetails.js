import React, { Component } from 'react';
import { connect } from 'react-redux';
import axios from 'axios';

import homeImg from '../../../gfx/home.png';
import Button from '../../UI/Button/Button';
import ModalEdit from './ModalEdit/ModalEdit';
import ModalBackground from '../../UI/ModalBackground/ModalBackground';
import Container from '../../../containers/Container/Container';

import classes from './CompanyDetails.css';

import * as actionCreators from '../../../store/actions/index';

class CompanyDetails extends Component {
    state = {
        modal: false
    }

    openEditModal = () => {
        this.setState({ modal: true });
    }

    saveData = async (companyName, email, address, NIP) => {
        try {
            const response = await axios({
                url: `http://localhost:5000/api/companies/edit`,
                method: 'put',
                headers: { 'Authorization': "Bearer " + this.props.auth_token },
                data: {
                    id: this.props.id,
                    companyName,
                    email,
                    address,
                    NIP
                }
            });

            if (response.status === 204) {
                this.props.editCompanyData(companyName, email, address, NIP);
                this.setState({ modal: false });
            }
        } catch (err) {
            console.log(err);
        }
    }

    cancelData = () => {
        this.setState({ modal: false });
    }

    render() {
        return (
            <Container>
                <div className={classes.CompanyDetails}>
                    <div className={classes.PhotoBg}><img src={homeImg} alt="user" className={classes.Photo} /></div>
                    <div className={classes.Content}>
                        <div className={classes.Details}>
                            <p className={classes.Data}> <span>Company name:</span> {this.props.companyName} </p>
                            <p className={classes.Data}> <span>NIP:</span> {this.props.companyNIP} </p>
                            <p className={classes.Data}> <span>Email:</span> {this.props.companyEmail} </p>
                            <p className={classes.Data}> <span>Address:</span> {this.props.companyAddress} </p>
                        </div>
                        <div className={classes.Buttons} >
                            <Button className={classes.Button} btnType="Cyan" clicked={this.openEditModal}> Edit details </Button>
                        </div>
                    </div>
                </div>

                {this.state.modal ? <ModalEdit onCancel={this.cancelData} onSave={this.saveData} /> : null}
                {this.state.modal ? <ModalBackground clicked={this.cancelData} /> : null}
            </Container>
        );
    }
}

const mapStateToProps = state => {
    return {
        companyName: state.company.companyName,
        companyEmail: state.company.companyEmail,
        companyAddress: state.company.companyAddress,
        companyNIP: state.company.companyNIP,
        id: state.company.companyId,
        auth_token: state.auth.token
    };
};

const mapDispatchToProps = dispatch => {
    return {
        editCompanyData: (companyName, email, address, NIP) => dispatch(actionCreators.editCompanyData(companyName, email, address, NIP))
    };
};

export default connect(mapStateToProps, mapDispatchToProps)(CompanyDetails);
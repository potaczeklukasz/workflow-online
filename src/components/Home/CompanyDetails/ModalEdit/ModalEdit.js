import React, { Component } from 'react';
import { connect } from 'react-redux';

import Button from '../../../UI/Button/Button';
import Input from '../../../UI/Input/Input';

import classes from './ModalEdit.css';

import { updateObject } from '../../../../helpers/helpers';

class ModalEdit extends Component {
    state = {
        controls: {
            companyName: {
                label: "Company name: ",
                elementConfig: {
                    type: 'input'
                },
                value: this.props.companyName
            },
            NIP: {
                label: "NIP: ",
                elementConfig: {
                    type: 'input'
                },
                value: this.props.NIP
            },
            email: {
                label: "Email: ",
                elementConfig: {
                    type: 'input'
                },
                value: this.props.email
            },
            address: {
                label: "Address: ",
                elementType: "textarea",
                elementConfig: {
                    type: 'email'
                },
                value: this.props.address
            },
        }
    }

    inputChangedHandler = (event, controlName) => {
        const updatedControls = updateObject(this.state.controls, {
            [controlName]: updateObject(this.state.controls[controlName], {
                value: event.target.value
            })
        });
        this.setState({ controls: updatedControls });
    }

    render() {
        const formElementsArray = [];
        for (let key in this.state.controls) {
            formElementsArray.push({
                id: key,
                config: this.state.controls[key]
            });
        }

        let form = formElementsArray.map(formElement => (
            <Input
                key={formElement.id}
                label={formElement.config.label}
                elementType={formElement.config.elementType}
                elementConfig={formElement.config.elementConfig}
                value={formElement.config.value}
                invalid={!formElement.config.valid}
                shouldValidate={formElement.config.validation}
                touched={formElement.config.touched}
                onChange={(event) => this.inputChangedHandler(event, formElement.id)} />
        ));

        return (
            <div className={classes.ModalEdit}>
                {form}
                <Button className={classes.Button} btnType="Green" clicked={this.props.onSave.bind(null, this.state.controls.companyName.value, this.state.controls.email.value, this.state.controls.address.value, this.state.controls.NIP.value)}> Save </Button>
                <Button className={classes.Button} btnType="Red" clicked={this.props.onCancel}> Cancel </Button>
            </div>
        )
    }
}

const mapStateToProps = state => {
    return {
        companyName: state.company.companyName,
        NIP: state.company.companyNIP,
        email: state.company.companyEmail,
        address: state.company.companyAddress
    };
};

export default connect(mapStateToProps)(ModalEdit);
import React from 'react';

import UserDetails from './UserDetails/UserDetails';
import CompanyDetails from './CompanyDetails/CompanyDetails';

import classes from './Home.css';

const home = () => {
    return (
        <div className={classes.Home}>
            <UserDetails />
            <CompanyDetails />
        </div>
    );
}

export default home;
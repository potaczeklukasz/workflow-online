import React, { Component } from 'react';

import Button from '../../UI/Button/Button';
import ConfirmModal from '../../UI/ConfirmModal/ConfirmModal';
import ModalBackground from '../../UI/ModalBackground/ModalBackground';

import classes from './Module.css';

class Module extends Component {
    state = {
        modal: false
    }

    showModal = () => {
        this.setState({ modal: true });
    }

    cancel = () => {
        this.setState({ modal: false });
    }

    activate = () => {
        this.props.confirm(this.props.id);
        this.setState({ modal: false });
    }

    deactivate = () => {
        this.props.confirm(this.props.purchasedId);
        this.setState({ modal: false });
    }

    render() {
        return (
            <div className={classes.Module}>
                <img src={`http://localhost:5000/api/images/${this.props.photo}`} alt={this.props.name} className={classes.ModuleImg} />
                <div className={classes.Box}>
                    <p className={classes.Name}> {this.props.name} </p>
                    <p className={classes.Description}> {this.props.description} </p>

                    {this.props.purchased ?
                        <Button className={classes.Button} btnType="Red" clicked={this.showModal}> Deactivate </Button> :
                        <Button className={classes.Button} btnType="Green" clicked={this.showModal}> Activate </Button>}

                    <Button className={classes.Button} btnType="Yellow"> Open </Button>

                    {this.state.modal ? <ConfirmModal onYes={this.props.purchased ? this.deactivate : this.activate} onNo={this.cancel} /> : null}
                    {this.state.modal ? <ModalBackground clicked={this.cancel} /> : null}
                </div>
            </div>
        );
    }
}

export default Module;
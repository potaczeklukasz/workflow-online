import React, { Component } from 'react';
import { connect } from 'react-redux';
import axios from 'axios';

import Module from './Module/Module';

import classes from './Modules.css';

import { compare } from '../../helpers/helpers';

class Modules extends Component {
    state = {
        modules: []
    }

    async componentDidMount() {
        try {
            const response = await axios({
                url: `http://localhost:5000/api/modules/${this.props.companyId}`,
                method: 'get',
                headers: { 'Authorization': "Bearer " + this.props.auth_token },
            });

            if (response.status === 200) {
                const modules = response.data.modules;
                const purchasedModules = response.data.purchasedModules;

                // adding information whether the module was activated for the user or not
                const combineModules = modules.map(_module => {
                    const index = purchasedModules.findIndex(x => x.moduleId === _module.id);

                    _module.purchased = (index !== -1) ? true : false;
                    if (index !== -1) _module.purchasedId = purchasedModules[index].id;

                    return _module;
                });
                combineModules.sort(compare);
                await this.setState({ modules: combineModules });
                console.log(this.state.modules);
            }
        } catch (err) {
            console.log(err);
        }
    }

    activateModule = async (id) => {
        try {
            const response = await axios({
                url: `http://localhost:5000/api/modules`,
                method: 'post',
                headers: { 'Authorization': "Bearer " + this.props.auth_token },
                data: {
                    moduleId: id,
                    companyId: this.props.companyId
                }
            });

            if (response.status === 200) {
                const modules = this.state.modules.map(_module => {
                    if (_module.id === id) {
                        _module.purchased = true;
                        _module.purchasedId = response.data.id
                    }
                    return _module;
                });
                this.setState({ modules: modules });
            }
        } catch (err) {
            console.log(err);
        }
    }

    deactivateModule = async (id) => {
        try {
            const response = await axios({
                url: `http://localhost:5000/api/modules`,
                method: 'delete',
                headers: { 'Authorization': "Bearer " + this.props.auth_token },
                data: { id }
            });

            if (response.status === 204) {
                const modules = this.state.modules.map(_module => {
                    if (_module.purchasedId === id) {
                        _module.purchased = false;
                        delete _module.purchasedId;
                    }
                    return _module;
                });
                this.setState({ modules: modules });
            }
        } catch (err) {
            console.log(err);
        }
    }

    render() {
        const modules = this.state.modules.map(_module => {
            return <Module
                key={_module.id}
                name={_module.name}
                id={_module.id}
                photo={_module.photoUrl}
                description={_module.description}
                purchased={_module.purchased}
                purchasedId={_module.purchasedId}
                confirm={_module.purchased ? this.deactivateModule : this.activateModule} />
        });

        return (
            <div className={classes.Modules}>
                {modules}
            </div>
        );
    }
}

const mapStateToProps = state => {
    return {
        companyId: state.company.companyId,
        auth_token: state.auth.token
    };
};

export default connect(mapStateToProps)(Modules);
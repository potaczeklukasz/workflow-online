import React from 'react';

import Button from '../../UI/Button/Button';

import classes from './User.css';

const user = (props) => {
    return (
        <div className={classes.User}>
            <p> <span> Surname: </span> {props.surname} </p>
            <p> <span> Name: </span> {props.name} </p>
            <p> <span> Position: </span> {props.position} </p>
            <Button className={classes.Button} btnType="Green" clicked={props.onShow}> Show details </Button>
        </div>
    );
}

export default user;
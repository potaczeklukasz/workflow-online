import React from 'react';

import classes from './SearchInput.css';

const searchInput = (props) => {
    return (
        <div className={classes.SearchInput}>
            <label className={classes.Label}>{props.label}
                <input
                    onChange={(event) => props.changed(event, props.name)}
                    type="text"
                    value={props.value} />
            </label>
        </div>
    );
}

export default searchInput;
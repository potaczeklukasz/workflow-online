import React from 'react';

import SearchInput from './SearchInput/SearchInput';
import Button from '../../UI/Button/Button';

import classes from './Search.css';

const search = (props) => {
    return (
        <div className={classes.Search}>
            <form onSubmit={(e) => props.onSearch(e)}>
                <SearchInput changed={props.changed} name="surname" value={props.search.surname} label="Surname:" />
                <SearchInput changed={props.changed} name="name" value={props.search.name} label="Name:" />
                <SearchInput changed={props.changed} name="position" value={props.search.position} label="Position:" />
                <Button className={classes.Button} btnType="Cyan" clicked={props.onSearch}> Search </Button>
                <Button className={classes.Button} btnType="Red" clicked={props.onClear}> Clear </Button>
            </form>
        </div>
    );
}

export default search;
import React, { Component } from 'react';

import Button from '../../../UI/Button/Button';
import Input from '../../../UI/Input/Input';

import classes from './ModalEdit.css';

import { updateObject } from '../../../../helpers/helpers';

class ModalEdit extends Component {
    state = {
        controls: {
            firstName: {
                label: "Name: ",
                elementConfig: {
                    type: 'input'
                },
                value: this.props.user.firstName
            },
            lastName: {
                label: "Surname: ",
                elementConfig: {
                    type: 'input'
                },
                value: this.props.user.lastName
            },
            position: {
                label: "Position: ",
                elementConfig: {
                    type: 'input'
                },
                value: this.props.user.position
            },
            email: {
                label: "Email: ",
                elementConfig: {
                    type: 'email'
                },
                value: this.props.user.email
            },
        }
    }

    inputChangedHandler = (event, controlName) => {
        const updatedControls = updateObject(this.state.controls, {
            [controlName]: updateObject(this.state.controls[controlName], {
                value: event.target.value
            })
        });
        this.setState({ controls: updatedControls });
    }

    render() {
        const formElementsArray = [];
        for (let key in this.state.controls) {
            formElementsArray.push({
                id: key,
                config: this.state.controls[key]
            });
        }

        let form = formElementsArray.map(formElement => (
            <Input
                key={formElement.id}
                label={formElement.config.label}
                elementType={formElement.config.elementType}
                elementConfig={formElement.config.elementConfig}
                value={formElement.config.value}
                invalid={!formElement.config.valid}
                shouldValidate={formElement.config.validation}
                touched={formElement.config.touched}
                onChange={(event) => this.inputChangedHandler(event, formElement.id)} />
        ));

        return (
            <div className={classes.ModalEdit}>
                {form}
                <Button
                    className={classes.Button}
                    btnType="Green"
                    clicked={this.props.onUpdate.bind(null, this.props.user.id, this.state.controls.firstName.value, this.state.controls.lastName.value, this.state.controls.position.value, this.state.controls.email.value)}> Save </Button>
                <Button className={classes.Button} btnType="Red" clicked={this.props.onCancel}> Cancel </Button>
            </div>
        )
    }
}

export default ModalEdit;
import React, { PureComponent } from 'react';
import { connect } from 'react-redux';
import axios from 'axios';

import userImg from '../../../gfx/user.png';
import Button from '../../UI/Button/Button';
import ModalEdit from './ModalEdit/ModalEdit';
import ConfirmModal from '../../UI/ConfirmModal/ConfirmModal';
import ModalBackground from '../../UI/ModalBackground/ModalBackground';

import classes from './UserDetails.css';

class UserDetails extends PureComponent {
    state = {
        editModal: false,
        confirmModal: false
    }

    // edit user details modal
    openEditModal = () => {
        this.setState({ editModal: true });
    }

    cancelData = () => {
        this.setState({ editModal: false });
    }

    saveData = async (id, firstName, lastName, position, email) => {
        this.props.onUpdate(id, firstName, lastName, position, email);
        try {
            const response = await axios({
                url: `http://localhost:5000/api/users`,
                method: 'put',
                headers: { 'Authorization': "Bearer " + this.props.auth_token },
                data: {
                    id: this.props.user.id,
                    firstName,
                    lastName,
                    position,
                    email
                }
            });

            if (response.status === 204) {
                this.setState({ editModal: false });
            }
        } catch (err) {
            console.log(err);
        }
    }

    // confirm delete modal
    openConfirmModal = () => {
        this.setState({ confirmModal: true });
    }

    confirmDelete = () => {
        this.props.onDelete(this.props.user.id);
        this.setState({ confirmModal: false });
    }

    cancelDelete = () => {
        this.setState({ confirmModal: false });
    }

    render() {
        let buttons = !(this.props.firstName === this.props.user.firstName && this.props.lastName === this.props.user.lastName && this.props.position === this.props.user.position);
        return (
            <div className={classes.UserDetails}>
                <img src={this.props.user.pictureUrl !== null ? `http://localhost:5000/api/images/${this.props.user.pictureUrl}` : userImg} alt="user" className={classes.Photo} />
                <div className={classes.Content}>
                    <div className={classes.Details}>
                        <p className={classes.Data}> <span>Name:</span> {this.props.user.firstName} </p>
                        <p className={classes.Data}> <span>Surname:</span> {this.props.user.lastName} </p>
                        <p className={classes.Data}> <span>Position:</span> {this.props.user.position} </p>
                        <p className={classes.Data}> <span>Email:</span> {this.props.user.email} </p>
                    </div>
                    <div className={classes.Buttons} >
                        <Button className={classes.Button} btnType="Yellow" clicked={this.props.onHide}> Hide details </Button>
                        {buttons ? <Button className={classes.Button} btnType="Blue" clicked={this.openEditModal}> Edit </Button> : null}
                        {buttons ? <Button className={classes.Button} btnType="Red" clicked={this.openConfirmModal}> Delete </Button> : null}
                    </div>
                </div>
                {this.state.editModal ? <ModalEdit onCancel={this.cancelData} onUpdate={this.saveData} user={this.props.user} /> : null}
                {this.state.editModal ? <ModalBackground clicked={this.cancelData} /> : null}

                {this.state.confirmModal ? <ConfirmModal onYes={this.confirmDelete} onNo={this.cancelDelete} /> : null}
                {this.state.confirmModal ? <ModalBackground clicked={this.cancelDelete} /> : null}
            </div>
        );
    }
}

const mapStateToProps = state => {
    return {
        auth_token: state.auth.token,
        firstName: state.auth.firstName,
        lastName: state.auth.lastName,
        position: state.auth.position
    };
};

export default connect(mapStateToProps)(UserDetails);
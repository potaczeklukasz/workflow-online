import React from 'react';

import Button from '../../UI/Button/Button';

import classes from './Pagination.css';

const pagination = (props) => {
    return (
        <div className={classes.Pagination}>
            <Button className={classes.Button} btnType="Green" clicked={props.onAdd} > Add new user </Button>

            <Button className={classes.PaginationButton} btnType="Cyan" clicked={props.onDown} > {"<"} </Button>
            <input className={classes.Input} type="number" value={props.currentPage} onChange={props.onSet}/>
            <Button className={classes.PaginationButton} btnType="Cyan" clicked={props.onUp} > {">"}  </Button>
            <p> of </p>
            <p className={classes.Number}> {props.totalPages} </p>
        </div>
    );
}

export default pagination;
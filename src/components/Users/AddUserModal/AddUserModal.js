import React, { Component } from 'react';
import { connect } from 'react-redux';
import axios from 'axios';

import { updateObject, checkValidity } from '../../../helpers/helpers';
import Input from '../../UI/Input/Input';
import Button from '../../UI/Button/Button';

import classes from './AddUserModal.css';

class AddUserModal extends Component {
    state = {
        controls: {
            login: {
                elementType: 'input',
                elementConfig: {
                    type: 'email',
                    placeholder: 'Mail address'
                },
                value: '',
                label: 'User details:',
                validation: {
                    required: true,
                    isEmail: true
                },
                valid: false,
                touched: false
            },
            password: {
                elementType: 'input',
                elementConfig: {
                    type: 'password',
                    placeholder: 'Password'
                },
                value: '',
                validation: {
                    required: true
                },
                valid: false,
                touched: false
            },
            firstName: {
                elementType: 'input',
                elementConfig: {
                    type: 'input',
                    placeholder: 'First name'
                },
                value: '',
                validation: {
                    required: true
                },
                valid: false,
                touched: false
            },
            lastName: {
                elementType: 'input',
                elementConfig: {
                    type: 'input',
                    placeholder: 'Last name'
                },
                value: '',
                validation: {
                    required: true
                },
                valid: false,
                touched: false
            },
            position: {
                elementType: 'input',
                elementConfig: {
                    type: 'input',
                    placeholder: 'Position'
                },
                value: '',
                validation: {
                    required: true
                },
                valid: false,
                touched: false
            }
        },
        error: null
    }

    inputChangedHandler = (event, controlName) => {
        const updatedControls = updateObject(this.state.controls, {
            [controlName]: updateObject(this.state.controls[controlName], {
                value: event.target.value,
                valid: checkValidity(event.target.value, this.state.controls[controlName].validation),
                touched: true
            })
        });
        this.setState({ controls: updatedControls });
    }

    addUser = async () => {
        if (this.checkIfValid()) {
            try {
                const response = await axios.post('http://localhost:5000/api/accounts/register', {
                    email: this.state.controls.login.value,
                    password: this.state.controls.password.value,
                    firstName: this.state.controls.firstName.value,
                    lastName: this.state.controls.lastName.value,
                    position: this.state.controls.position.value,
                    companyId: this.props.companyId
                });

                if (response.status === 201) {
                    this.props.hide();
                    this.props.search();
                }
            } catch (err) {
                console.log(err.response.data)
                let errors = "";
                for (const e in err.response.data) {
                    errors += `${err.response.data[e]} \n`;
                }
                this.setState({ error: errors });
            }
        } else {
            this.setState({ error: "Please fill in all fields" });
        }
    }

    checkIfValid = () => {
        let isValid = true;
        for (const input in this.state.controls) {
            if (!this.state.controls[input].valid) isValid = false;
        }
        return isValid;
    }

    render() {
        const formElementsArray = [];
        for (let key in this.state.controls) {
            formElementsArray.push({
                id: key,
                config: this.state.controls[key]
            });
        }

        let form = formElementsArray.map(formElement => (
            <Input
                label={formElement.config.label}
                key={formElement.id}
                elementType={formElement.config.elementType}
                elementConfig={formElement.config.elementConfig}
                value={formElement.config.value}
                invalid={!formElement.config.valid}
                shouldValidate={formElement.config.validation}
                touched={formElement.config.touched}
                onChange={(event) => this.inputChangedHandler(event, formElement.id)} />
        ));

        return (
            <div className={classes.AddUserModal}>
                {form}
                <Button btnType="Green" clicked={this.addUser}>Add user</Button>
                <Button btnType="Red" clicked={this.props.hide}>Cancel</Button>
                <p className={classes.Error}>{this.state.error}</p>
            </div>
        );
    }
}

const mapStateToProps = state => {
    return {
        companyId: state.company.companyId
    };
};

export default connect(mapStateToProps)(AddUserModal);
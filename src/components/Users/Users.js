import React, { Component } from 'react';
import { connect } from 'react-redux';
import axios from 'axios';

import Search from './Search/Search';
import User from './User/User';
import UserDetails from './UserDetails/UserDetails';
import Pagination from './Pagination/Pagination';
import AddUserModal from './AddUserModal/AddUserModal';
import ModalBackground from '../UI/ModalBackground/ModalBackground';

import classes from './Users.css';

import { updateObject } from '../../helpers/helpers';

class Users extends Component {
    state = {
        totalPages: null,
        currentPage: 1,
        users: [],
        search: {
            name: "",
            surname: "",
            position: ""
        },
        addUserModal: false
    }

    componentDidMount() {
        this.search();
    }

    // pagination
    pageUp = async () => {
        if (this.state.currentPage < this.state.totalPages) {
            await this.setState((prevState, props) => ({
                currentPage: prevState.currentPage + 1
            }));
            this.search();
        }
    };

    pageDown = async () => {
        if (this.state.currentPage > 1) {
            await this.setState((prevState, props) => ({
                currentPage: prevState.currentPage - 1
            }));
            this.search();
        } else if (this.state.currentPage === "") {
            await this.setState((prevState, props) => ({
                currentPage: 1
            }));
            this.search();
        }
    };

    setPage = async (e) => {
        const value = e.target.value;
        if ((value <= this.state.totalPages && value > 0) || value === "") {
            await this.setState({ currentPage: value });
            if (value !== "") this.search();
        }
    }

    // searching
    inputChangedHandler = (event, searchName) => {
        const updatedSearch = updateObject(this.state.search, {
            [searchName]: event.target.value
        });
        this.setState({ search: updatedSearch });
    }

    clearSearch = async () => {
        await this.setState({
            search: {
                name: "",
                surname: "",
                position: ""
            }
        });
        this.search();
    }

    search = async (e) => {
        if (e) e.preventDefault();
        try {
            const response = await axios({
                url: `http://localhost:5000/api/users`,
                method: 'post',
                headers: { 'Authorization': "Bearer " + this.props.auth_token },
                data: {
                    companyId: this.props.companyId,
                    firstName: this.state.search.name,
                    lastName: this.state.search.surname,
                    position: this.state.search.position,
                    page: this.state.currentPage
                }
            });
            if (response.status === 200) {
                const users = response.data.values.map(user => {
                    user.showDetails = false;
                    return user;
                });
                this.setState({ users: users, totalPages: response.data.info.pages, currentPage: response.data.info.currentPage });
            }
        } catch (err) {
            console.log(err);
        }
    }

    // displaying
    showDetails = (id) => {
        const users = this.state.users.map(user => {
            if (user.id === id) {
                user.showDetails = true;
            }
            return user;
        });
        this.setState({ users: users });
    }

    hideDetails = (id) => {
        const users = this.state.users.map(user => {
            if (user.id === id) {
                user.showDetails = false;
            }
            return user;
        });
        this.setState({ users: users });
    }

    // update state
    updateUsers = (id, name, surname, position, email) => {
        const users = this.state.users.map(user => {
            if (user.id === id) {
                user.firstName = name;
                user.lastName = surname;
                user.position = position;
                user.email = email;
            }
            return user;
        });
        this.setState({ users: users });
    }

    deleteUser = async (id) => {
        try {
            const response = await axios({
                url: `http://localhost:5000/api/users/${id}`,
                method: 'delete',
                headers: { 'Authorization': "Bearer " + this.props.auth_token }
            });
            if (response.status === 204) {
                // const users = this.state.users;
                // users.splice(this.state.users.findIndex(x => x.id === id), 1);
                // this.setState({ users: users });
                this.search();
            }
        } catch (err) {
            console.log(err);
        }
    }

    // add user modal
    showModal = () => this.setState({ addUserModal: true });

    hideModal = () => this.setState({ addUserModal: false });

    render() {
        const users = this.state.users.map(user => {
            if (user.showDetails) {
                return <UserDetails
                    key={user.id}
                    user={user}
                    onHide={this.hideDetails.bind(null, user.id)}
                    onUpdate={this.updateUsers}
                    onDelete={this.deleteUser} />;
            } else {
                return <User
                    key={user.id}
                    name={user.firstName}
                    surname={user.lastName}
                    position={user.position}
                    onShow={this.showDetails.bind(null, user.id)} />;
            }
        });

        return (
            <div className={classes.Users}>
                <Search changed={this.inputChangedHandler} search={this.state.search} onSearch={this.search} onClear={this.clearSearch} />
                {users}
                <Pagination
                    currentPage={this.state.currentPage}
                    totalPages={this.state.totalPages}
                    onUp={this.pageUp}
                    onDown={this.pageDown}
                    onSet={this.setPage}
                    onAdd={this.showModal} />

                {this.state.addUserModal ? <AddUserModal hide={this.hideModal} search={this.search} /> : null}
                {this.state.addUserModal ? <ModalBackground clicked={this.hideModal} /> : null}
            </div>
        );
    }
}

const mapStateToProps = state => {
    return {
        auth_token: state.auth.token,
        companyId: state.company.companyId
    };
};

export default connect(mapStateToProps)(Users);
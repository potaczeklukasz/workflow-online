import React from 'react';

import Button from '../Button/Button';

import classes from './ConfirmModal.css';

const confirmModal = (props) => {
    return (
        <div className={classes.ConfirmModal}>
            <p> Are you sure? </p>
            <Button btnType="Green" clicked={props.onYes}> Yes </Button>
            <Button btnType="Red" clicked={props.onNo}> No </Button>
        </div>
    );
}

export default confirmModal;
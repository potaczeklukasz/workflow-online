import React from 'react';

import classes from './ModalBackground.css';

const modalBackground = (props) => (
    <div className={classes.ModalBackground} onClick={props.clicked}> </div>
);

export default modalBackground;
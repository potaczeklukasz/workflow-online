import React, { Component } from 'react';
import { connect } from 'react-redux';
import { Redirect, withRouter, Link } from 'react-router-dom';
import axios from 'axios';

import Input from '../UI/Input/Input';
import Button from '../UI/Button/Button';
import JustifyCenter from '../../containers/JustifyCenter/JustifyCenter';

import classes from './Login.css';

import { updateObject, checkValidity } from "../../helpers/helpers";
import { saveCookie, getCookies } from '../../helpers/cookies';
import * as actionCreators from '../../store/actions/index';

class Login extends Component {
    state = {
        controls: {
            login: {
                elementType: 'input',
                elementConfig: {
                    type: 'email',
                    placeholder: 'Mail address'
                },
                value: '',
                validation: {
                    required: true,
                    isEmail: true
                },
                valid: false,
                touched: false
            },
            password: {
                elementType: 'input',
                elementConfig: {
                    type: 'password',
                    placeholder: 'Password'
                },
                value: '',
                validation: {
                    required: true
                },
                valid: false,
                touched: false
            }
        },
        error: null
    }

    inputChangedHandler = (event, controlName) => {
        const updatedControls = updateObject(this.state.controls, {
            [controlName]: updateObject(this.state.controls[controlName], {
                value: event.target.value,
                valid: checkValidity(event.target.value, this.state.controls[controlName].validation),
                touched: true
            })
        });
        this.setState({ controls: updatedControls });
    }

    submitHandler = async (event) => {
        event.preventDefault();
        if (this.state.controls.login.valid === true && this.state.controls.password.valid === true) {
            try {
                const response = await axios.post('http://localhost:5000/api/auth/login', {
                    userName: this.state.controls.login.value,
                    password: this.state.controls.password.value
                });

                if (response.status === 200) {
                    this.props.login(response.data.auth_token, response.data.id);
                    saveCookie(response.data.auth_token, response.data.id, 2);
                }
            } catch (err) {
                this.setState({ error: err.response.data.error });
            }
        } else {
            this.setState({ error: "Invalid username or password" });
        }
    }

    render() {
        const cookies = getCookies();
        const formElementsArray = [];
        for (let key in this.state.controls) {
            formElementsArray.push({
                id: key,
                config: this.state.controls[key]
            });
        }

        let form = formElementsArray.map(formElement => (
            <Input
                key={formElement.id}
                elementType={formElement.config.elementType}
                elementConfig={formElement.config.elementConfig}
                value={formElement.config.value}
                invalid={!formElement.config.valid}
                shouldValidate={formElement.config.validation}
                touched={formElement.config.touched}
                onChange={(event) => this.inputChangedHandler(event, formElement.id)} />
        ));

        return (
            <JustifyCenter>
                <div className={classes.Login}>
                    {this.props.isAuthenticated || cookies.auth_token ? <Redirect to="/" /> : null}
                    <form onSubmit={this.submitHandler}>
                        {form}
                        <Button btnType="Green">Login</Button>
                        <p className={classes.Error}>{this.state.error}</p>
                        <Link className={classes.Link} to="/password"> I forgot my password </Link>
                    </form>
                </div>
            </JustifyCenter>
        );
    }
}

const mapStateToProps = state => {
    return {
        isAuthenticated: state.auth.token !== null
    };
};

const mapDispatchToProps = dispatch => {
    return {
        login: (token, userId) => dispatch(actionCreators.login(token, userId))
    };
};

export default withRouter(connect(mapStateToProps, mapDispatchToProps)(Login));
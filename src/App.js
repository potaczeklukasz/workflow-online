import React, { Component } from 'react';
import { Route, Switch, withRouter, Redirect } from 'react-router-dom';
import { connect } from 'react-redux';
import './App.css';

import Login from './components/Login/Login';
import Header from './components/Header/Header';
import Register from './components/Register/Register';
import Sidebar from './components/Sidebar/Sidebar';
import Home from './components/Home/Home';
import Users from './components/Users/Users';
import Modules from './components/Modules/Modules';
import Password from './components/Password/Password';
import PasswordReset from './components/Password/PasswordReset/PasswordReset';
import PasswordCancel from './components/Password/PasswordCancel/PasswordCancel';

import classes from './App.css';

import { getCookies } from './helpers/cookies';
import * as actionCreators from './store/actions/index';

class App extends Component {
	state = {
		showSidebar: window.innerWidth < 950 ? false : true
	}

	toggleMenu = () => {
		this.state.showSidebar ? this.setState({ showSidebar: false }) : this.setState({ showSidebar: true });
	}

	componentDidMount() {
		const cookies = getCookies();
		if (cookies.auth_token) this.props.setTokenFromCookie(cookies.auth_token, cookies.userId);
		window.addEventListener("resize", () => {
			window.innerWidth < 950 ? this.setState({ showSidebar: false }) : this.setState({ showSidebar: true });
		});
	}

	render() {
		let routes = (
			<Switch>
				<Route path="/login" component={Login} />
				<Route path="/register" component={Register} />
				<Route path="/password/cancel" component={PasswordCancel} />
				<Route path="/password/reset" component={PasswordReset} />
				<Route path="/password" component={Password} />
				<Redirect to="login" />
			</Switch>
		);

		if (this.props.isAuthenticated) {
			routes = (
				<Switch>
					<Route path="/" exact component={Home} />
					<Route path="/users" component={Users} />
					<Route path="/modules" component={Modules} />
					<Redirect to="/" />
				</Switch>
			);
		}

		return (
			<div className={classes.App}>
				<Header onClick={this.toggleMenu} />
				{!this.props.isAuthenticated ? routes : null}
				<div className={classes.Body}>
					{this.props.isAuthenticated ? <Sidebar style={!this.state.showSidebar ? { display: "none" } : null} /> : null}
					<div className={classes.Content}>
						{this.props.isAuthenticated ? routes : null}
					</div>
				</div>
			</div>
		);
	}
}

const mapStateToProps = state => {
	return {
		isAuthenticated: state.auth.token !== null
	};
};

const mapDispatchToProps = dispatch => {
	return {
		setTokenFromCookie: (auth_token, userId) => dispatch(actionCreators.setTokenFromCookie(auth_token, userId))
	};
};

export default withRouter(connect(mapStateToProps, mapDispatchToProps)(App));
